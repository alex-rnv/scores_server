package com.alexrnv.king.entity

import spock.lang.Specification

/**
 * @author: Alex
 * @version: 0.1
 */
class UserTest extends Specification {

    def Levels levels = new Levels();
    def Users users = new Users(levels);

    def "Default user rating for level is 0"() {
        def user = users.get(0)

        expect:
        user.highestScore(x) == 0

        where:
        x << [0, 100, Integer.MAX_VALUE]
    }

    def "User id must be non-negative"() {
        when:
        users.get(-1)

        then:
        thrown(IllegalArgumentException)
    }

    def "Highest score should be recalculated after add"() {
        def user = users.get(0)
        setup:
        user.addScore(0, 1)
        user.addScore(0, 2)
        user.addScore(0, 3)

        when:
        user.addScore(0, x)

        then:
        user.highestScore(0) == y

        where:
        x << [2, 3, 4]
        y << [3, 3, 4]

    }

    def "To rating should return string in following format"() {
        def user = users.get(42)

        setup:
        user.addScore(1, 4)

        when:
        user.addScore(x, y)

        then:
        user.toRating(x).equals(z)

        where:
        x << [0,      0,      1,      1]
        y << [1,      2,      3,      5]
        z << ["42=1", "42=2", "42=4", "42=5"]

    }
}
