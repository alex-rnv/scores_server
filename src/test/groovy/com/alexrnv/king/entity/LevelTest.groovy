package com.alexrnv.king.entity

import spock.lang.Specification

/**
 * @author: Alex
 * @version: 0.1
 */
class LevelTest extends Specification {

    def "No users scored after creation"() {
        def level = new Level(0, 10)

        expect:
        level.ratingSize() == 10
        level.usersScored() == 0
    }

    def "Level id must be non-negative"() {
        def levels = new Levels()

        when:
        levels.get(-1)

        then:
        thrown(IllegalArgumentException)
    }

    def "Null user references are forbidden"() {
        def level = new Level(0, 10)

        when:
        level.addUser(null)

        then:
        thrown(NullPointerException)
    }

    def "Adding user"() {
        def level = new Level(0, 10)

        setup:
        level.addUser(new User(0, null))
        level.addUser(new User(1, null))

        when:
        level.addUser(new User(id, null))

        then:
        level.usersScored() == length

        where:
        id  |  length
        0   |  2
        1   |  2
        2   |  3
        3   |  3
    }

    def "Removing user"() {
        def level = new Level(0, 10)

        setup:
        level.addUser(new User(0, null))
        level.addUser(new User(1, null))
        level.addUser(new User(2, null))

        when:
        level.removeUser(new User(id, null))

        then:
        level.usersScored() == length

        where:
        id  |  length
        0   |  2
        1   |  2
        9   |  3
        8   |  3
    }

}
