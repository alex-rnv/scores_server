package com.alexrnv.king.entity

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

/**
 * @author: Alex
 * @version: 0.1
 */
@Stepwise
class UsersTest extends Specification {

    @Shared
    def levels = new Levels()
    @Shared
    def users = new Users(levels)

    def "No users in th list"() {
        expect:
        users.size() == 0
    }

    def "New user created"() {
        when:
        users.get(0)

        then:
        users.size() == 1
    }

    def "Another user created"() {
        when:
        def user0 = users.get(0)
        def user1 = users.get(1)

        then:
        user0.id != user1.id
        users.size() == 2
    }

}
