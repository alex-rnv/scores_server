package com.alexrnv.king.entity

import spock.lang.Shared
import spock.lang.Stepwise

/**
 * @author: Alex
 * @version: 0.1
 */
@Stepwise
class LevelsTest extends spock.lang.Specification {

    @Shared
    def levels = new Levels()

    def "No levels in the list"() {
        expect:
        levels.size() == 0
    }

    def "Insert level first time"() {
        when:
        def lvl = levels.get(0)

        then:
        levels.size() == 1
        lvl.id == 0
        lvl.ratingSize() == Levels.MAX_RATINGS
    }

    def "Retrieve level second time"() {
        when:
        levels.get(0)

        then:
        levels.size() == 1
    }

    def "Insert another level"() {
        when:
        def lvl2 = levels.get(1)

        then:
        levels.size() == 2
        lvl2.id != levels.get(0).id
    }
}
