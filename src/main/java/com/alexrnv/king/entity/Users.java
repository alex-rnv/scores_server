package com.alexrnv.king.entity;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author: Alex
 * @version: 0.1
 */
public class Users {

    private static final int MAX_USERS = 10000;

    private final Levels levels;

    private final ConcurrentMap<Integer, User> users = new ConcurrentHashMap<>(MAX_USERS);

    public Users(Levels levels) {
        this.levels = levels;
    }

    public User get(int id) {
        return users.computeIfAbsent(id, n -> new User(n, levels));
    }

    public int size() {
        return users.size();
    }
}
