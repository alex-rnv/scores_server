package com.alexrnv.king.entity;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicReference;

/**
 * The main idea behind the class is that it stores some pre-calculated sorted data for rating requests. The only
 * sorted concurrent set implementation provided by JDK is {@link ConcurrentSkipListSet}. Here are some conclusions
 * and trade-offs:
 * 1)we should consider only highest users scores.
 * 2)we store all user scores, not only N required for responses to http clients (this is what we do in real apps usually).
 * 3)but we also store and update some pre-calculated http response data, taking following assumptions into account:
 *  - the head of the rating are updated rare
 *  - we can have a flag which states if we need to recalculate rating response next time it is used;
 * @author: Alex
 * @version: 0.1
 */
public class Level {
    private final int id;
    private final int ratingSize;//number of items in rating response
    private final ConcurrentSkipListSet<User> data;//users sorted by high score
    private final Comparator<User> userComparator;
    private volatile boolean update = true;//states that response should be recalculated
    private final AtomicReference<User> lastRating = new AtomicReference<>();//user with position {ratingSize} in the rating
    private volatile String response = "";//pre-calculated rating response

    public Level(int id, int ratingSize) {
        if(id < 0) throw new IllegalArgumentException();
        this.id = id;
        this.ratingSize = ratingSize;
        this.userComparator = (o1, o2) -> {
            if(o1.getId() == o2.getId()) {
                return 0;//to support removal by id
            } else {
                int v = o2.highestScore(id) - o1.highestScore(id);
                return v != 0 ? v : (o2.getId() - o1.getId());//use ids to order users with equal scores
            }
        };

        this.data = new ConcurrentSkipListSet<>(userComparator);
    }

    public int getId() {
        return id;
    }

    public int ratingSize() {
        return ratingSize;
    }

    public void addUser(User user) {
        data.add(user);
        if(data.size() >= ratingSize) {
            lastRating.compareAndSet(null, user);
            //move lastRating up, todo resolve races
            Optional.ofNullable(lastRating.get())
                    .filter(p -> userComparator.compare(p, user) >= 0)
                    .ifPresent(u -> {
                        lastRating.compareAndSet(u, data.higher(u));
                        update = true;
                    });
        }
    }

    public void removeUser(User user) {
        data.remove(user);
    }

    public int usersScored() {
        return data.size();
    }

    public String highScores() {
        if(update) {
            if (data.isEmpty())
                response = "";

            Iterator<User> iterator = data.iterator();
            StringBuilder sb = new StringBuilder().append(iterator.next().toRating(id));
            int rs = 0;
            while (iterator.hasNext() && rs++ < ratingSize) {
                sb.append(",").append(iterator.next().toRating(id));
            }
            response = sb.toString();
            update = false;
        }
        return response;
    }
}
