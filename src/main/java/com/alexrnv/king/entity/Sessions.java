package com.alexrnv.king.entity;

import java.util.UUID;
import java.util.concurrent.*;

/**
 * @author: Alex
 * @version: 0.1
 */
public class Sessions {

    private final int DELAY_SECONDS = 600;
    private final int SESSIONS_CAPACITY = 100;

    private final ScheduledExecutorService cleaner = Executors.newScheduledThreadPool(4);
    /**
     * Maps session UID to user. Allows several sessions per user.
     */
    private final ConcurrentMap<String, User> sessions = new ConcurrentHashMap<>(SESSIONS_CAPACITY);

    public String newSession(User user) {
        String session = generateSessionUID(user);
        sessions.putIfAbsent(session, user);
        cleaner.schedule(() -> sessions.remove(session), DELAY_SECONDS, TimeUnit.SECONDS);
        return session;
    }

    public User getUser(String session) {
        return sessions.get(session);
    }

    private String generateSessionUID(User user) {
        return UUID.randomUUID().toString();
    }

}
