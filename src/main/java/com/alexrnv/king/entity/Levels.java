package com.alexrnv.king.entity;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Class provides facade to {@link Level}s. New levels are lazy-created on demand.
 * Since number of levels is bounded, using pre-allocated hash based table seems reasonable.
 *
 * @author: Alex
 * @version: 0.1
 */
public class Levels {
    static final int MAX_LEVELS = 100;
    static final int MAX_RATINGS = 15;

    private final ConcurrentMap<Integer, Level> levels = new ConcurrentHashMap<>(MAX_LEVELS);

    public Level get(int id) {
        return levels.computeIfAbsent(id, n -> new Level(n, MAX_RATINGS) );
    }

    public int size() {
        return levels.size();
    }
}
