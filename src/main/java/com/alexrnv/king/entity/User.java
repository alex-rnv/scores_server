package com.alexrnv.king.entity;

import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @author: Alex
 * @version: 0.1
 */
public class User {

    /**
     * Average user scores per level
     */
    private static final int LEVEL_ATTEMPTS = 11;

    private final int id;
    private final Levels levels;

    /**
     * Holding all scores for levels in sorted queue, to simplify high score retrieval. Usage of {@link PriorityBlockingQueue}
     * is a trade-off, the easiest approach. What we really need here is a data structure which maintains following
     * invariant - head is the biggest element, the rest could be unsorted.
     * Initial capacity of {@link Levels#MAX_LEVELS} is maximum size. It could be adjusted according to average levels number
     * passed by arbitrary user, to make some good compromise between memory consumption and map resizing.
     */
    private final ConcurrentMap<Integer, PriorityBlockingQueue<Integer>> scoresByLevel = new ConcurrentHashMap<>(Levels.MAX_LEVELS);

    private final Comparator<Integer> reverseComparator = (o1, o2) -> o2 - o1;

    public User(int id, Levels levels) {
        if(id < 0) throw new IllegalArgumentException();
        this.id = id;
        this.levels = levels;
    }

    /**
     * Retrieves user's identifier
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Adds user's score for level.
     * @param level - level id
     * @param score - score amount
     * @return true if added score is the biggest for the level
     */
    public boolean addScore(final int level, final int score) {
        PriorityBlockingQueue<Integer> queue = scoresByLevel.computeIfAbsent(level,
                n -> {
                    PriorityBlockingQueue<Integer> p = new PriorityBlockingQueue<>(LEVEL_ATTEMPTS, reverseComparator);
                    p.add(0);//add 0 value in advance to avoid null issues
                    return p;
                });
        boolean h = score > queue.peek();//rebuild level flag
        if(h) {
            Level lvl = levels.get(level);
            synchronized (queue) {//this monitor will guarantee user is not modified from another thread while re-inserting in the level tree
                h = score > queue.peek();//dcl to prevent races when posting scores for the same user concurrently
                if(h) {
                    lvl.removeUser(this);
                    queue.add(score);
                    lvl.addUser(this);
                }
            }
        } else {
            queue.add(score);
        }
        return h;
    }

    /**
     * Retrieves highest score for level
     * @param level - level id
     * @return Highest score amount or 0 if level is not passed
     */
    public int highestScore(final int level) {
        PriorityBlockingQueue<Integer> queue = scoresByLevel.get(level);
        return queue != null ? queue.peek() : 0;
    }

    public String toRating(final int lvl) {
        return id + "=" + highestScore(lvl);
    }
}
