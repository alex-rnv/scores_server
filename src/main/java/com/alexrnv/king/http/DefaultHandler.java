package com.alexrnv.king.http;

import com.alexrnv.king.entity.*;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

/**
 * @author: Alex
 * @version: 0.1
 */
public class DefaultHandler implements HttpHandler {

    private final Levels levels = new Levels();
    private final Users users = new Users(levels);
    private final Sessions sessions = new Sessions();

    @Override
    public void handle(HttpExchange t) throws IOException {
        try {
            URI uri = t.getRequestURI();
            String[] path = uri.getPath().replaceFirst("/", "").split("/");
            Integer id = null;
            try {
                id = Integer.valueOf(path[0]);
            } catch (NumberFormatException e) {
                responseError(t, "Number id expected, actual: " + path[0]);
            }
            String cmd = path[1];
            switch(cmd) {
                case "login":
                    handleLogin(t, id);
                    break;
                case "score":
                    Headers headers = t.getRequestHeaders();
                    String sessionKey = headers.getFirst("sessionkey");
                    int score = readInt(t);
                    handlePostScore(t, sessionKey, id, score);
                    break;
                case "highscorelist":
                    handleGetRating(t, id);
                    break;
                default:
                    responseError(t, "Unknown request type: " + cmd);
            }
        } catch (IOException e) {
           throw e;
        } catch(Exception e) {
            responseError(t, "Server error: " + e.getMessage());
        }
    }

    public void handleLogin(HttpExchange t, int usr) throws IOException {
        User user = users.get(usr);
        String session = sessions.newSession(user);
        responseOk(t, session);
    }

    public void handlePostScore(HttpExchange t, String ssn, int lvl, int score) throws IOException {
        User user = sessions.getUser(ssn);
        if(user != null) {
            user.addScore(lvl, score);
        }
        responseOk(t, "");
    }

    public void handleGetRating(HttpExchange t, int lvl) throws IOException {
        Level level = levels.get(lvl);
        String result = level.highScores();
        responseOk(t, result);
    }

    protected void responseOk(HttpExchange t, String response) throws IOException {
        response(t, response, 200);
    }

    protected void responseError(HttpExchange t, String response) throws IOException {
        response(t, response, 500);
    }

    protected void response(HttpExchange t, String response, int code) throws IOException {
        t.sendResponseHeaders(code, response.length());
        try (OutputStream os = t.getResponseBody()) {
            os.write(response.getBytes());
        } catch (IOException e) {
            throw e;
        }
    }

    private int readInt(HttpExchange t) throws IOException {
        DataInputStream is = new DataInputStream(t.getRequestBody());
        return is.readInt();
    }

}
