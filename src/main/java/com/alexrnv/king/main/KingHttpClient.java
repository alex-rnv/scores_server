package com.alexrnv.king.main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

/**
 * @author: Alex
 * @version: 0.1
 */
public class KingHttpClient {

    private final String loginUrl;
    private final String postScoreUrl;
    private final String highScoreListUrl;

    public KingHttpClient(String host, int port) {
        this.loginUrl = "http://" + host + ":" + port + "/#userID#/login";
        this.postScoreUrl = "http://" + host + ":" + port + "/#levelID#/score";
        this.highScoreListUrl = "http://" + host + ":" + port + "/#levelID#/highscorelist";
    }

    public static void main(String[] args) throws IOException {
        final int USERS_SIZE = 100;
        final int LEVELS_SIZE = 10;
        final int MAX_LEVEL_ATTEMPTS_PER_USER = 3;
        final int MAX_SCORE = 1000;
        KingHttpClient client = new KingHttpClient("localhost", 8686);

        Random userIDs = new Random();
        Set<String> sessions = Collections.newSetFromMap(new ConcurrentHashMap<>(USERS_SIZE));
        userIDs.ints(USERS_SIZE, 1, USERS_SIZE).forEach(userID -> sessions.add(client.login(userID)));

        sessions.parallelStream().forEach(session ->
                IntStream.range(1, LEVELS_SIZE).forEach(level ->
                    IntStream.range(0, MAX_LEVEL_ATTEMPTS_PER_USER).forEach(attempt ->
                        client.postScore(session, level, ThreadLocalRandom.current().nextInt(MAX_SCORE))
                    )
                )
        );

        IntStream.range(1, LEVELS_SIZE).forEach(level -> {
            System.out.println(client.highScoreList(level));
        });
    }

    public String login(int userID) {
        try {
            URL url = new URL(loginUrl.replace("#userID#", String.valueOf(userID)));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDefaultUseCaches(true);
            try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                return in.readLine();
            } catch (IOException e) {
                throw e;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    public int postScore(String session, int level, int score) {
        try {
            URL url = new URL(postScoreUrl.replace("#levelID#", String.valueOf(level)));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDefaultUseCaches(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("sessionkey", session);
            try (DataOutputStream out = new DataOutputStream(connection.getOutputStream())) {
                out.writeInt(score);
            } catch (IOException e) {
                throw e;
            }
            return connection.getResponseCode();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    public String highScoreList(int levelID)  {
        try {
            URL url = new URL(highScoreListUrl.replace("#levelID#", String.valueOf(levelID)));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDefaultUseCaches(true);
            try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                return in.readLine();
            } catch (IOException e) {
                throw e;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }
}
