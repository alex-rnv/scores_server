package com.alexrnv.king.main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * @author: Alex
 * @version: 0.1
 */
public class LoadClient {

    private static final int USERS_NUM = 1000;
    private static final int LEVELS_NUM = 100;
    private static final int MAX_USER_SCORES = 100;
    private static final int MAX_SCORE = 10000;

    private final ExecutorService executor;
    private final KingHttpClient client;

    public LoadClient(ExecutorService executor, KingHttpClient client) {
        this.executor = executor;
        this.client = client;
    }

    public void run() throws InterruptedException {
        for(int u = 0; u < USERS_NUM; u++) {
            executor.submit(new LoginTask(client, executor, u));
        }
        Thread.sleep(1000);
        List<HighScoresTask> tasks = new ArrayList<>(LEVELS_NUM);
        for(int l = 0; l < LEVELS_NUM; l++) {
            tasks.add(new HighScoresTask(client, l));
        }
        Collection<Future<String>> results = executor.invokeAll(tasks);
        for(Future f : results) {
            try {
                f.get();
            } catch (ExecutionException e) {
                System.err.println(e.getMessage());
            }
        }
        executor.shutdownNow();
    }

    public static void main(String[] args) throws InterruptedException {
        LoadClient lc = new LoadClient(Executors.newFixedThreadPool(10), new KingHttpClient("localhost", 8686));
        lc.run();
    }

    private static class LoginTask implements Callable<String>{
        private final KingHttpClient client;
        private final ExecutorService executor;
        private final int userID;

        private LoginTask(KingHttpClient client, ExecutorService executor, int userID) {
            this.client = client;
            this.executor = executor;
            this.userID = userID;
        }

        @Override
        public String call() throws Exception {
            String session = client.login(userID);
            //System.out.println("Logged in: " + session);
            Random random = ThreadLocalRandom.current();
            for(int i = 0; i < random.nextInt(MAX_USER_SCORES); i++) {
                executor.submit(new PostScoreTask(client, session, random.nextInt(LEVELS_NUM), random.nextInt(MAX_SCORE)));
            }
            return session;
        }
    }

    private static class PostScoreTask implements Callable<String>{
        private final KingHttpClient client;
        private final String session;
        private final int level;
        private final int score;

        private PostScoreTask(KingHttpClient client, String session, int level, int score) {
            this.client = client;
            this.session = session;
            this.level = level;
            this.score = score;
        }

        @Override
        public String call() throws Exception {
            int r = client.postScore(session, level, score);
            //System.out.printf("Posted score %d for level %d : %d\n",  score, level, r);
            return "";
        }
    }

    private static class HighScoresTask implements Callable<String>{
        private final KingHttpClient client;
        private final int level;

        private HighScoresTask(KingHttpClient client, int level) {
            this.client = client;
            this.level = level;
        }

        @Override
        public String call() throws Exception {
            String list = client.highScoreList(level);
            System.out.printf("Level %d high score list: %s\n", level, list);
            return list;
        }
    }
}
