package com.alexrnv.king.main;

import com.alexrnv.king.http.DefaultHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * @author: Alex
 * @version: 0.1
 */
public class KingHttpServer {
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8686), 0);
        server.createContext("/", new DefaultHandler());
        server.setExecutor(null);//todo
        server.start();
    }
}
